Proyecto base educativo sobre la arquitecura de React

## Instalación

Clonar repositorio y ejecutar los siguientes scripts:

### `npm install`

Instalación de las dependencias necesarias para el funcionamiento de la app.<br>

### `npm start`

Inicia la app en ambiente de desarrollo.<br>
Abrir [http://localhost:3000](http://localhost:3000) para ver en el navegador.

Al realizar cambios, el sitio se refresca para poder verlos.<br>
Puedes ver los errores en la consola del navegador.

### `npm run build`

Prepara la app para producción en la carpeta `build`.<br>
Este es el bundle correcto para subir a producción, optimiza para tener una mejor perfomance de la app.

Tu app esta lista para el deploy!

Mas información [deployment](https://facebook.github.io/create-react-app/docs/deployment)

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Analizar el tamaño del bundle

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Crear aplicaciones progresivas

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Configuración avanzada

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
