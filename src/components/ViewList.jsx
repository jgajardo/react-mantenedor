import React, { Component } from 'react'
import Cabecera from './Cabecera'
import Lista from './Lista'

export default class ViewList extends Component {
    render() {
        //Capturamos "data" desde el componente padre y tambien capturamos "handleClick" desde componente hijo
        const { data, handleClick, nuevoUsuario } = this.props
        return (
            <div>
                <Cabecera nuevoUsuario={nuevoUsuario} />
                {/*Volvemos a pasar data al componente hijo*/}
                <Lista data={data} handleClick={handleClick} />
            </div>
        )
    }
}