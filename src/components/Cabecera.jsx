import React, { Component } from 'react'

const styles = {
    inline: {
        display: 'inline'
    }
}

export default class Cabecera extends Component {
    render() {
        const { nuevoUsuario } = this.props
        return (
            <header>
                <h2>Usuarios</h2>
                <button onClick={nuevoUsuario}>nuevo usuario</button>
            </header>
        )
    }
}