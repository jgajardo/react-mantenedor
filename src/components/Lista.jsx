import React, { Component } from 'react'

export default class Lista extends Component {
    //Metodo que captura id para pasarselo a componente padre
    handleClick = id => e => {
        const { handleClick } = this.props
        handleClick(id)
    }
    render() {
        //Por fin sacamos data en el componente hijo nuevamente
        const { data } = this.props
        return (
           <ul>
              {data.map(x =>
                <li key={x.id}>{x.name}<button onClick={this.handleClick(x.id)}>Editar</button></li>)}
           </ul>
        )
    }
    //PD:Es muy importante siempre pasar el "key" en los componentes de react, como en el caso del <li> que tenemos 
}